#!/bin/sh
cd /home/$USER/Downloads
if  [[ $1 = "-u" ]]; then
    wget "https://releases.ubuntu.com/21.04/ubuntu-21.04-desktop-amd64.iso"
else
    echo "Please choose a parameter: -u (Ubuntu) -l (Lubuntu) -f (Fedora) -a (Arch) -d (Debian)"
fi
if  [[ $1 = "-l" ]]; then
    wget "http://cdimage.ubuntu.com/lubuntu/releases/18.04/release/lubuntu-18.04-alternate-amd64.iso"
else
    echo "Please choose a parameter: -u (Ubuntu) -l (Lubuntu) -f (Fedora) -a (Arch) -d (Debian)"
fi
if  [[ $1 = "-f" ]]; then
    wget "https://download.fedoraproject.org/pub/fedora/linux/releases/34/Workstation/x86_64/iso/Fedora-Workstation-Live-x86_64-34-1.2.iso"
else
    echo "Please choose a parameter: -u (Ubuntu) -l (Lubuntu) -f (Fedora) -a (Arch) -d (Debian)"
fi
if  [[ $1 = "-a" ]]; then
    wget "https://mirror.arizona.edu/archlinux/iso/2021.05.01/archlinux-2021.05.01-x86_64.iso"
else
    echo "Please choose a parameter: -u (Ubuntu) -l (Lubuntu) -f (Fedora) -a (Arch) -d (Debian)"
fi
if  [[ $1 = "-d" ]]; then
    wget "https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-10.9.0-amd64-netinst.iso"
else
    echo "Please choose a parameter: -u (Ubuntu) -l (Lubuntu) -f (Fedora) -a (Arch) -d (Debian)"
fi
