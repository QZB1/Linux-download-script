# Linux-download-script
It's a script for downloading different flavours of linux.

run by using the following:

```
./linux-downloader.sh -X
```
the different tags are: -u (Ubuntu) -l (Lubuntu) -f (Fedora) -a (Arch) -d (Debian)

I'll try to keep the script as updated as possible.
NOTE: you may have to make it executable by using

```
sudo chmod +x linux-downloader.sh
```
the image will download in your downloads directory.
